Proekt po predmetot Softverski kvalitet i testiranje:

Slavica Gabreshanec 151085,
Viktorija Ristova 151068.



Testirana e Spring Boot web aplikacija (coffee shop). 

Funkcionalnosta na nekolkute funkcii sto gi nudi se testirani so Selenium, Mockito i JUnit.

Testovite se naogjaat vo /src/test/java/com/viki/SpringcoffeeshopApplicationTests.java

Application starts at http://localhost:8080