package com.viki;

import com.viki.model.Product;
import com.viki.repository.CustomerRepository;
import com.viki.repository.OrderRepository;
import com.viki.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcoffeeshopApplication {

    public static void main(String[] args) {
		SpringApplication.run(SpringcoffeeshopApplication.class, args);
	}



}
