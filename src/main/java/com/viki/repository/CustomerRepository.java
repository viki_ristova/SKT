package com.viki.repository;

import com.viki.model.Customer;
import org.springframework.data.repository.CrudRepository;


/**
 * Created by Viki Ristova on 10/06/18.
 */

public interface CustomerRepository extends CrudRepository<Customer, Long>{

}
