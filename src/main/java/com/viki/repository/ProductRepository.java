package com.viki.repository;

import com.viki.model.Product;
import org.springframework.data.repository.CrudRepository;


/**
 * Created by Viki Ristova on 10/06/18.
 */

public interface ProductRepository extends CrudRepository<Product, Long> {

}




