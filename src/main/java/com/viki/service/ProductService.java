package com.viki.service;

import com.viki.model.Customer;
import com.viki.model.CustomerOrder;
import com.viki.model.Product;
import com.viki.repository.CustomerRepository;
import com.viki.repository.OrderRepository;
import com.viki.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService implements CommandLineRunner {
    @Autowired
    ProductRepository productRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    public ProductService(CustomerRepository customerRepository,OrderRepository orderRepository){
        this.customerRepository=customerRepository;
        this.orderRepository=orderRepository;
    }
    public List<CustomerOrder> getAllOrders(){

        return (List<CustomerOrder>)orderRepository.findAll();
    }
  /*  public Customer getAllOrdersByCustomerID(Long id){
        return orderRepository.findAllOrdersByCustomerId(id);
    }*/
    /*
    public CustomerOrder addOrder(String name) {
        CustomerOrder order = new CustomerOrder();

    }*/
    public CustomerOrder deleteOrder(Long id){
        CustomerOrder order=orderRepository.findOne(id);
        if(order!=null){
            this.customerRepository.delete(id);
            return order;
        }
        else{throw new DataRetrievalFailureException("Order not found!");}

    }
    @Override
    public void run(String... strings) throws Exception {

        Product mocha = new Product();
        mocha.setProductName("Mocha");
        mocha.setProductPrice(3.95);

        Product capuccinno = new Product();
        capuccinno.setProductName("Capuccinno");
        capuccinno.setProductPrice(4.95);

        productRepository.save(mocha);
        productRepository.save(capuccinno);


    }

}
