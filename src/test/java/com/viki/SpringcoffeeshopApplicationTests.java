package com.viki;

import com.viki.model.Customer;
import com.viki.model.CustomerOrder;
import com.viki.model.Product;
import com.viki.repository.CustomerRepository;
import com.viki.repository.OrderRepository;
import com.viki.service.ProductService;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.junit.Assert.*;
import static org.junit.gen5.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringcoffeeshopApplicationTests {
    public static WebDriver driver;
    public static String baseUrl;

    @Test
    public void contextLoads() {

    }

    //MOCK TESTING
    private ProductService productService;
    private OrderRepository orderRepositoryMock;
    private CustomerRepository customerRepositoryMock;

    @Before
    public void setup() {
        customerRepositoryMock = Mockito.mock(CustomerRepository.class);
        orderRepositoryMock = Mockito.mock(OrderRepository.class);
        productService = new ProductService(customerRepositoryMock, orderRepositoryMock);

    }
    @DisplayName("Testing getAllOrders from service logic, which should result with returning all orders from the persistence layer")
    @Test
    public void getAllOrdersTest(){
        CustomerOrder customerOrder=new CustomerOrder();
        Customer customer=new Customer();
        customer.setFirstName("MArko");
        customer.setLastName("Markovski");
        customer.setCustomerId(7);
        customerOrder.setCustomer(customer);
        Product product=new Product();
        product.setProductName("Latte");
        product.setProductPrice(2.56);
        product.setProductId((long) 15);
        Set<Product> products=new HashSet<>();
        products.add(product);
        customerOrder.setProducts(products);
        customerOrder.setTotal((double) 1);
        customerOrder.setOrderId((long) 3);
        orderRepositoryMock.save(customerOrder);
        Mockito.when(orderRepositoryMock.findOne(customerOrder.getOrderId())).thenReturn(customerOrder);
        //2
        CustomerOrder customerOrder2=new CustomerOrder();
        Customer customer2=new Customer();
        customer.setFirstName("Ana");
        customer.setLastName("Anevska");
        customer.setCustomerId(8);
        customerOrder.setCustomer(customer2);
        Product product2=new Product();
        product.setProductName("Machiato");
        product.setProductPrice(3.35);
        product.setProductId((long) 15);
        Set<Product> products2=new HashSet<>();
        products2.add(product2);
        customerOrder2.setProducts(products2);
        customerOrder2.setTotal((double) 1);
        customerOrder2.setOrderId((long) 4);
        orderRepositoryMock.save(customerOrder2);
        Mockito.when(orderRepositoryMock.findOne(customerOrder2.getOrderId())).thenReturn(customerOrder2);
        when(productService.getAllOrders()).thenReturn(Arrays.asList(customerOrder,customerOrder2));


        assertNotNull(orderRepositoryMock.findOne((long) 3));
        assertEquals(orderRepositoryMock.findOne((long) 3).toString(),customerOrder.toString());
        assertEquals(productService.getAllOrders().size(),2);
        assertEquals(productService.getAllOrders(),Arrays.asList(customerOrder,customerOrder2));
        assertEquals(orderRepositoryMock.findOne((long) 4),customerOrder2);
    }
    @DisplayName("Testing deleteOrder from service logic which should result with the order when it is deleted, or an exception if an order with the given id doesn't exist")
    @Test
    public void deleteOrderByIdTest(){
        CustomerOrder customerOrder=new CustomerOrder();
        Customer customer=new Customer();
        customer.setFirstName("MArko");
        customer.setLastName("Markovski");
        customer.setCustomerId(7);
        customerOrder.setCustomer(customer);
        Product product=new Product();
        product.setProductName("Latte");
        product.setProductPrice(2.56);
        product.setProductId((long) 15);
        Set<Product> products=new HashSet<>();
        products.add(product);
        customerOrder.setProducts(products);
        customerOrder.setTotal((double) 1);
        customerOrder.setOrderId((long) 3);
        orderRepositoryMock.save(customerOrder);
        Mockito.when(orderRepositoryMock.findOne(customerOrder.getOrderId())).thenReturn(customerOrder);
        //2
        CustomerOrder customerOrder2=new CustomerOrder();
        Customer customer2=new Customer();
        customer.setFirstName("Ana");
        customer.setLastName("Anevska");
        customer.setCustomerId(8);
        customerOrder.setCustomer(customer2);
        Product product2=new Product();
        product.setProductName("Machiato");
        product.setProductPrice(3.35);
        product.setProductId((long) 15);
        Set<Product> products2=new HashSet<>();
        products2.add(product2);
        customerOrder2.setProducts(products2);
        customerOrder2.setTotal((double) 1);
        customerOrder2.setOrderId((long) 4);
        orderRepositoryMock.save(customerOrder2);
        Mockito.when(orderRepositoryMock.findOne(customerOrder2.getOrderId())).thenReturn(customerOrder2);
        when(productService.getAllOrders()).thenReturn(Arrays.asList(customerOrder,customerOrder2));

        assertEquals(productService.deleteOrder(customerOrder.getOrderId()),customerOrder);
        assertThrows(DataRetrievalFailureException.class,()->productService.deleteOrder((long) 90));
    }


    //AUTOMATED TESTING WITH SELENIUM
    @Test
    public void AFirstTestAddingProductTest() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("http://localhost:8080/products");
        WebElement element = driver.findElement(By.cssSelector("a.navbar-brand"));
        element.click();
        Thread.sleep(3000);
        WebElement element2 = driver.findElement(By.id("product_name"));
        //	element2.click();
        element2.sendKeys("espresso");
        Thread.sleep(3000);
        WebElement element3 = driver.findElement(By.id("product_price"));
        //	element3.click();
        element3.sendKeys("1.5");
        Thread.sleep(3000);
        WebElement element4 = driver.findElement(By.id("btn_submit"));
        element4.click();
        Thread.sleep(7000);
        try {
            //assertTrue(driver.switchTo().alert().getText().equals("Product with Id 3 Saved"));
            Thread.sleep(9000);
            //WebElement element5 = driver.findElement(By.id("btn_submit"));
            //element5.sendKeys(Keys.chord(Keys.ESCAPE));
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            assertEquals(alertText, "Product with Id 3 Saved");
            alert.dismiss();
        } catch (org.openqa.selenium.UnhandledAlertException e) {
            System.out.print("CATCH ");
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            assertEquals(alertText, "Product with Id 3 Saved");
            alert.dismiss();
        } catch (org.openqa.selenium.NoAlertPresentException ex) {
            System.out.print("CATCH ");
            Thread.sleep(3000);

        }

    }

    @Test
    public void EmptyProductNameTest() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("http://localhost:8080/");
        WebElement element1 = driver.findElement(By.id("product_price"));
        element1.sendKeys("2.64");
        Thread.sleep(3000);
        WebElement element2 = driver.findElement(By.id("btn_submit"));
        element2.click();
        Thread.sleep(7000);
        try {
            Thread.sleep(3000);
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            assertEquals(alertText, "Product name cannot be empty");
            alert.dismiss();
        } catch (org.openqa.selenium.UnhandledAlertException e) {
            System.out.print("CATCH ");
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            assertEquals(alertText, "Product name cannot be empty");
            alert.dismiss();
        }
    }


    @Test
    public void emptyProductsFields() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chromedriver.exe");
        driver = new ChromeDriver();
        Thread.sleep(7000);
        driver.get("http://localhost:8080/products");
        Thread.sleep(7000);
        WebElement element2 = driver.findElement(By.id("product_name"));
        //element2.click();
        element2.sendKeys("");
        Thread.sleep(7000);
        WebElement element3 = driver.findElement(By.id("product_price"));
        //element3.click();
        element3.sendKeys("");
        Thread.sleep(7000);
        WebElement element4 = driver.findElement(By.id("btn_submit"));
        element4.click();
        Thread.sleep(7000);
        try {
            //assertTrue(driver.switchTo().alert().getText().equals("Product with Id 3 Saved"));
            Thread.sleep(7000);
            //WebElement element5 = driver.findElement(By.id("btn_submit"));
            //element5.sendKeys(Keys.chord(Keys.ESCAPE));
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            assertEquals(alertText, "Product name cannot be empty");
            alert.dismiss();
        } catch (org.openqa.selenium.UnhandledAlertException e) {
            System.out.print("CATCH ");
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            assertEquals(alertText, "Product name cannot be empty");
            assertEquals(alertText, "Product name cannot be empty");
            alert.dismiss();
        }
    }

    @Test
    public void emptyFirstNameTest() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("http://localhost:8080/orders");
        WebElement element = driver.findElement(By.id("1"));
        element.click();
        Thread.sleep(3000);
        WebElement element2 = driver.findElement(By.id("customer_last_name"));
        element2.sendKeys("Ristova");
        WebElement element3 = driver.findElement(By.id("btn_submit"));
        element3.click();
        try {
            Thread.sleep(3000);
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            assertEquals(alertText, "You must fill the first name");
            alert.dismiss();
        } catch (org.openqa.selenium.UnhandledAlertException e) {
            System.out.print("CATCH ");
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            assertEquals(alertText, "You must fill the first name");
            alert.dismiss();
        }
    }

    @Test
    public void filledCredentialsButEmptyOrder() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("http://localhost:8080/orders");
        WebElement element1 = driver.findElement(By.id("customer_first_name"));
        element1.sendKeys("Marko");
        Thread.sleep(3000);
        WebElement element2 = driver.findElement(By.id("customer_last_name"));
        element2.sendKeys("Markovski");
        WebElement element3 = driver.findElement(By.id("btn_submit"));
        element3.click();
        try {
            Thread.sleep(3000);
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            assertEquals(alertText, "You must at least select one product");
            alert.dismiss();
        } catch (org.openqa.selenium.UnhandledAlertException e) {
            System.out.print("CATCH ");
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            assertEquals(alertText, "You must at least select one product");
            alert.dismiss();
        }
    }
    @Test
    public void BemptyProductPrice() throws InterruptedException{
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chromedriver.exe");
        driver=new ChromeDriver();
        Thread.sleep(7000);
        driver.get("http://localhost:8080/products");
        Thread.sleep(7000);
        WebElement element2 = driver.findElement(By.id("product_name"));
        //element2.click();
        element2.sendKeys("Latte");
        Thread.sleep(7000);
        WebElement element3=driver.findElement(By.id("product_price"));
        //element3.click();
        element3.sendKeys("");
        Thread.sleep(7000);
        WebElement element4 = driver.findElement(By.id("btn_submit"));
        element4.click();
        Thread.sleep(7000);
        try {
            Thread.sleep(6000);
            Alert alert=driver.switchTo().alert();
            String alertText1=alert.getText();
            assertEquals(alertText1,"Product with Id 4 Saved");
            alert.dismiss();
        }
        catch(org.openqa.selenium.UnhandledAlertException e){
          //  System.out.print("CATCH ");
            Alert alert=driver.switchTo().alert();
            String alertText =alert.getText();
            assertEquals(alertText,"Product with Id 4 Saved");
            //assertEquals(alertText,"Product with Id 3 Saved");
            alert.dismiss();
        }
    }

}
